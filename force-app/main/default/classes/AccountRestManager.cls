@RestResource(urlMapping='/Account/*')
global with sharing class AccountRestManager {

    @HttpGet
    global static Account getAccountById() {
        RestRequest req = RestContext.request;
        String acctId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        Account acct = [SELECT Name
                            FROM Account
                           WHERE Id = :acctId];
        return acct;
    }

    @HttpPost
    global static ID createAccount() {
        Account acct = new Account(
            // Subject=subject,
            // Status=status,
            // Origin=origin,
            // Priority=priority
            );
        insert acct;
        return acct.Id;
    }   

    @HttpDelete
    global static void deleteAccount() {
        RestRequest req = RestContext.request;
        String acctId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        Account acct = [SELECT Id FROM Account WHERE Id = :acctId];
        delete acct;
    }     

    @HttpPut
    global static ID upsertAccount() {

        Account acct = new Account(
                // Id=id,
                // Subject=subject,
                // Status=status,
                // Origin=origin,
                // Priority=priority
            );
        // Match Account by Id, if present. Otherwise, create new Account.
        upsert acct;
        return acct.Id;
    }

    @HttpPatch
    global static ID updateAccountFields() {
        RestRequest req = RestContext.request;
        String acctId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        Account acct = [SELECT Id FROM Account WHERE Id = :acctId];

        // Deserialize the JSON string into name-value pairs
        Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestbody.tostring());
        // Iterate through each parameter field and value
        for(String fieldName : params.keySet()) {
            // Set the field and value on the Account sObject
            acct.put(fieldName, params.get(fieldName));
        }
        update acct;
        return acct.Id;
    }    
}